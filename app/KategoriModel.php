<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class KategoriModel extends Model
{
    public $table="tb_kategori";
    public $timestamps = false;
    protected $fillable = 
    ['id', 'nama_kategori', 'deskripsi'];

    //relasi one to  many
    public function get_barang(){
    return $this->hasMany('App\Barang','id_kategori','id_kategori');
      }

}