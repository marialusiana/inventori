<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaksi;
use App\Detail;
use App\Barang;
use App\Http\Requests;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;


class DetailController extends Controller
{
    function index(){
    
        Redis::Connection();
        $penjualandetail=Redis::get('penjualandetail');
        if ($penjualandetail !=null) {
          return $penjualandetail;
         }  
          $penjualandetail=Redis::set('penjualandetail',Detail::all());
          return Detail::all();

    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'id_transaksi'=>'numeric',
            'id_barang'=>'numeric',
            'harga_jual'=>'numeric',
            'jumlah_jual'=>'numeric',
        ]);

        $id = $request->input('id');
        $id_transaksi = $request->input('id_transaksi');
        $id_barang = $request->input('id_barang');
        $harga_jual = $request->input('harga_jual');
        $jumlah_jual = $request->input('jumlah_jual');

        $data = new \App\Detail();
        
        $data->id = $id;
        $data->id_transaksi = $id_transaksi;
        $data->id_barang = $id_barang;
        $data->harga_jual = $harga_jual;
        $data->jumlah_jual = $jumlah_jual;
        $data->subtotal = $jumlah_jual*$harga_jual;

        $barang=Barang::find($id_barang);
        $transaksi=Transaksi::find($id_transaksi);
    
        if ($transaksi){
           
            if($barang){

                if($data->save()){
                    Redis::del('penjualandetail');
                    $res['message'] = "Success!";
                    $res['value'] = "$data";
                    //return response($data);
            
                    return response()->json($data, 200);
                }
                return response()->json(['status' => 'error', 'message' => 'ID barang not found'],404);
           }
                return response()->json(['status' => 'error', 'message' => 'ID Nota barang not found'],404);
        }

    }

    public function show($id)
    {
        $result= Redis::get('penjualandetail:'.$id);
        if ($result) {
            return $result;
        }else{
            $result = Detail::find($id);

            $barang=Redis::set('penjualandetail:'.$id,$result);
            return response()->json(['status' => 'success', 'data' => $result]);
        }
    }

    public function update(Request $request,$id)
    {
        
        $this->validate($request,[
            'id_transaksi'=>'numeric',
            'id_barang'=>'numeric',
            'harga_jual'=>'numeric',
            'jumlah_jual'=>'numeric',
        ]);

        $id_barang = $request->input('id_barang');
        $id_transaksi=$request->input('id_transaksi');

        $barang=Barang::find($id_barang);
        $transaksi=Transaksi::find($id_transaksi);

        $detail = Detail::find($id);
        if(!$detail){
            return response()->json(['status' => 'error', 'message' => 'ID Detail not found'],404);
        }
        if ($transaksi){
           
            if($barang){
            Redis::del('penjualandetail:'.$id);
            Redis::del('penjualandetail');
            $detail->update($request->all());
            return response()->json($detail, 200);

           }
           return response()->json(['status' => 'error', 'message' => 'ID barang not found'],404);
        }    
        return response()->json(['status' => 'error', 'message' => 'ID Transaksi not found'],404);
    }

    public function destroy($id)
    {
      $detail = Detail::find($id);
      if ($detail) {
        Redis::del('penjualandetail:'.$id);
        Redis::del('penjualandetail');
        $detail->delete();
        return response()->json(['stats' => 'success', 'message' => 'Data has been deleted']);
      }
 
      return response()->json(['status' => 'error', 'message' => 'Cannot deleting data'],400);
    }


}

