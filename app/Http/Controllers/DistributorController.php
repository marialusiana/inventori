<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Distributor;
use App\Http\Requests;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;


class DistributorController extends Controller
{
    function index(){
    
        Redis::Connection();
        $distributor=Redis::get('distributor');
       if ($distributor !=null) {
          return $distributor;
         }  
          $distributor=Redis::set('distributor',Distributor::all());
          return Distributor::all();
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'nama_distributor'=>'required|max:255',
            'alamat'=>'required|max:255',
            'kota'=>'required|max:100',
            'email'=>'email',
            'telepon'=>'numeric',
        ]);
            Redis::del('distributor');
            $distibutor = Distributor::create($request->all()); 
            return response()->json($distibutor, 201);  
    }

    public function show($id)
    {
        $result= Redis::get('distributor:'.$id);
        if ($result) {
            return $result;
        }else{
            $result = Distributor::find($id);

            $distributor=Redis::set('distributor:'.$id,$result);
            return response()->json(['status' => 'success', 'data' => $result]);
        }
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'nama_distributor'=>'required|max:255',
            'alamat'=>'required|max:255',
            'kota'=>'required|max:100',
            'email'=>'email',
            'telepon'=>'numeric',
        ]);
        
        $distributor = Distributor::find($id);
        if(!$distributor){
            return response()->json(['status' => 'error', 'message' => 'ID Distributor not found'],404);
        }
            Redis::del('distributor');
            Redis::del('distributor:'.$id);
            $distributor->update($request->all()); 
            return response()->json($distributor, 200);
    }

    public function destroy($id)
    {
      $distributor = Distributor::find($id);
      if ($distributor) {
        Redis::del('distributor:'.$id);
        Redis::del('distributor');
        $distibutor->delete();
        return response()->json(['stats' => 'success', 'message' => 'Data has been deleted']);
      }
 
      return response()->json(['status' => 'error', 'message' => 'Cannot deleting data'],400);
    }
}
