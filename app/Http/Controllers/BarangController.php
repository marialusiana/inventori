<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Barang;
use App\KategoriModel;
use App\Http\Requests;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;


class BarangController extends Controller
{  

    function index(){

        Redis::Connection();
        $barang=Redis::get('barang');
       
       if ($barang) {
          return $barang; 
         }  
         $minute=10;
          Redis::setex('barang',$minute ,Barang::all());
          return Barang::all();
    }
           
  
    public function store(Request $request)
    {
        $this->validate($request,[
            'nama_barang'=>'required|max:255',
            'harga'=>'numeric|min:1',
            'id_kategori'=>'numeric',
        ]);
    
        $id_kategori = $request->input('id_kategori');
        $kategori=KategoriModel::find($id_kategori);

        if ($kategori){
            Redis::del('barang');
            $barang = Barang::create($request->all()); 
                return response()->json($barang, 201);  
            }
                return response()->json(['status' => 'error', 'message' => 'ID kategori not found'],404);
    }

    public function show($id)
    {
        $result = Redis::Connection();
        
        $result= Redis::get('barang:'.$id);
        if ($result) {
            return $result;
        }else{
            $result = Barang::find($id);

            $barang=Redis::set('barang:'.$id,$result);
            return response()->json(['status' => 'success', 'data' => $result]);
        }
    }


    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'nama_barang'=>'required|max:255',
            'harga'=>'numeric|min:1',
            'id_kategori'=>'numeric',
        ]);
        
        $id_kategori = $request->input('id_kategori');

        $kategori = KategoriModel::find($id_kategori);

        $barang = Barang::find($id);
        if(!$barang){
            return response()->json(['status' => 'error', 'message' => 'ID Barang not found'],404);
        }
            if ($kategori){
                Redis::del('barang:'.$id);
                Redis::del('barang');
                $barang->update($request->all()); 
                return response()->json($barang, 200);  
            }
                return response()->json(['status' => 'error', 'message' => 'ID kategori not found'],404);
    }

    public function destroy($id)
    {
      $barang = Barang::find($id);

      if ($barang) {
        Redis::del('barang:'.$id);
        Redis::del('barang');
        $barang->delete();
        return response()->json(['stats' => 'success', 'message' => 'Data has been deleted']);
      }
        return response()->json(['status' => 'error', 'message' => 'Cannot deleting data'],400);
    }
}

