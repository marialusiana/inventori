<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaksi;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Cache;



class TransaksiController extends Controller
{
    function index(){

        $result = Redis::Connection();
        $result= Redis::get('transaksi');
        if ($result) {
            return $result;
        }else{
                $result=DB::table('transaksi')
                ->select('transaksi.id',DB::raw('sum(if(transaksi.id=transaksi_detail.id_transaksi,subtotal,0)) as total_bayar'))
                ->join('transaksi_detail','transaksi.id','=','transaksi_detail.id_transaksi')
                ->groupBy('transaksi.id')
                ->get();
           
            $transaksi=Redis::set('transaksi',$result);
            return response()->json(['status' => 'success', 'data' => $result]);
        }
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'tgl_transaksi'=>'date',
        ]);
        Redis::del('transaksi');
        $transaksi = Transaksi::create($request->all());
         
        return response()->json($transaksi, 201);      
     
    }

    public function show($id)
    {
        $result = Redis::Connection();
        
        $result= Redis::get('transaksi:'.$id);
        if ($result) {
            return response()->json(['status' => 'success', 'data' => $result]);
        }else{
            $transaksi=Transaksi::find($id);
            if($transaksi){
                $result= DB::table('transaksi')
                ->select(DB::raw('transaksi.id,transaksi.tgl_transaksi,sum(if(transaksi.id=transaksi_detail.id_transaksi,subtotal,0)) as total_bayar'))
                 ->join('transaksi_detail','transaksi.id','=','transaksi_detail.id_transaksi')
                 ->where('transaksi.id','=', $id)
                 ->get();
    
                $transaksi=Redis::set('transaksi:'.$id,$result);
                return response()->json(['status' => 'success', 'data' => $result]);
            }
            return response()->json(['status' => 'error', 'message' => 'ID Transaksi not found'],404);
        }
 
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'tgl_transaksi'=>'date',
        ]);

        $transaksi = Transaksi::find($id);
        if(!$transaksi){
            
            return response()->json(['status' => 'error', 'message' => 'ID Transaksi not found'],404);
        }
        Redis::del('transaksi:'.$id);
        Redis::del('transaksi');
        $transaksi->update($request->all());
        return response()->json($transaksi, 200);
    }

    public function destroy($id)
    {
      $transaksi = Transaksi::find($id);
      if ($transaksi) {
        Redis::del('transaksi:'.$id);
        Redis::del('transaksi');
        $transaksi->delete();
        return response()->json(['status' => 'success', 'message' => 'Data has been deleted']);
      }
 
      return response()->json(['status' => 'error', 'message' => 'Cannot deleting data'],400);
    }


}

