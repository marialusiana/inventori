<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MasukDetail;
use App\Barang;
use App\BarangMasuk;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Cache;


class MasukDetailController extends Controller
{
    function index(){

        Redis::Connection();
        $pembeliandetail=Redis::get('pembeliandetail');
       
       if ($pembeliandetail !=null) {
          return $pembeliandetail;
         }  
          $pembeliandetail=Redis::set('pembeliandetail',MasukDetail::all());
          return MasukDetail::all();
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'id_notabarang'=>'numeric',
            'id_barang'=>'numeric',
            'harga'=>'numeric',
            'jumlah'=>'numeric',
        ]);
 
        $id = $request->input('id');
        $id_notabarang = $request->input('id_notabarang');
        $id_barang = $request->input('id_barang');
        $harga = $request->input('harga');
        $jumlah = $request->input('jumlah');
       // $subtotal = $request->input('subtotal');

        $data = new \App\MasukDetail();
        
        $data->id = $id;
        $data->id_notabarang = $id_notabarang;
        $data->id_barang = $id_barang;
        $data->harga = $harga;
        $data->jumlah = $jumlah;
        $data->subtotal = $jumlah*$harga;

        $barang=Barang::find($id_barang);
        $barangmasuk=BarangMasuk::find($id_notabarang);
    
        if ($barangmasuk){
           
            if($barang){

                if($data->save()){
                    Redis::del('pembeliandetail');
                    $res['message'] = "Success!";
                    $res['value'] = "$data";
                    //return response($data);
            
                    return response()->json($data, 200);
                }
                return response()->json(['status' => 'error', 'message' => 'ID barang not found'],404);
           }
                return response()->json(['status' => 'error', 'message' => 'ID Nota barang not found'],404);
        }    
        
    }

    public function show($id)
    {
        $result= Redis::get('pembeliandetail:'.$id);
        if ($result) {
            return $result;
        }else{
            $result = MasukDetail::find($id);

            $pembeliandetail=Redis::set('pembeliandetail:'.$id,$result);
            return response()->json(['status' => 'success', 'data' => $result]);
        }
    }

  
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'id_notabarang'=>'numeric',
            'id_barang'=>'numeric',
            'harga'=>'numeric',
            'jumlah'=>'numeric',
        ]);

        $id_barang = $request->input('id_barang');
        $id_notabarang=$request->input('id_notabarang');

        $barang=Barang::find($id_barang);
        $barangmasuk=BarangMasuk::find($id_notabarang);
        $masukdetail=MasukDetail::find($id);

        if(!$masukdetail){
            return response()->json(['status' => 'error', 'message' => 'ID Pembelian detail not found'],404);
        }
        if ($barangmasuk){
           
            if($barang){
            Redis::del('pembeliandetail');
            Redis::del('pembeliandetail:'.$id);
            $masukdetail->update($request->all());
            return response()->json($masukdetail, 200);

           }
           return response()->json(['status' => 'error', 'message' => 'ID barang not found'],404);
        }    
        return response()->json(['status' => 'error', 'message' => 'ID Transaksi not found'],404);
    }

    public function destroy($id)
    {
      $masukdetail = Masukdetail::find($id);
      if ($masukdetail) {
        Redis::del('pembeliandetail');
        Redis::del('pembeliandetail:'.$id);
        $masukdetail->delete();
        return response()->json(['stats' => 'success', 'message' => 'Data has been deleted']);
      }
 
      return response()->json(['status' => 'error', 'message' => 'Cannot deleting data'],400);
    }
}

