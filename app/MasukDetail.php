<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\BarangMasuk;
use App\MasukDetail;


class MasukDetail extends Model
{
    public $table="tb_detail_masuk";
    public $timestamps = false;
    protected $fillable = 
    ['id', 'id_nota_barang', 'id_barang','harga','jumlah','subtotal'];

    //relasi many to one
    public function get_barangmasuk(){
    return $this->belongsTo('App\DetailMasuk','id','id');
}

}