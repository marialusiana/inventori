<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Transaksi;

class Detail extends Model
{
    public $table="transaksi_detail";
    public $timestamps = false;
    protected $fillable = 
    ['id', 'id_transaksi', 'id_barang','harga_jual','jumlah_jual','subtotal'];

    //relasi one to  many
    public function get_detail(){
    return $this->hasMany('App\Detail','id_transaksi','id');
    
    }

}