<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
<<<<<<< HEAD

Route::post('login', 'API\UserController@login');
Route::post('register', 'API\UserController@register');

Route::group(['middleware' => 'auth:api'], function(){
    Route::post('details', 'API\UserController@details');
    
    //Route::resource('barang','BarangController',['except' => ['edit']]);
});

Route::middleware('auth:api')->resource('barang','BarangController',['except' => ['edit']]);
Route::middleware('auth:api')->resource('kategori','KategoriController',['except' => ['edit']]);
Route::middleware('auth:api')->resource('transaksi','TransaksiController',['except' => ['edit']]);
Route::middleware('auth:api')->resource('detail','DetailController',['except' => ['edit']]);

Route::middleware('auth:api')->resource('distributor','DistributorController',['except' => ['edit']]);

Route::middleware('auth:api')->resource('barangmasuk','BarangMasukController',['except' => ['edit']]);
Route::middleware('auth:api')->resource('masukdetail','MasukDetailController',['except' => ['edit']]);
Route::middleware('auth:api')->resource('distributor','DistributorController',['except' => ['edit']]);
=======
>>>>>>> meong
